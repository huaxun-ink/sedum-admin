package ink.huaxun.admin.menu.enumeration;

import ink.huaxun.core.enumeration.BaseEnum;

/**
 * 菜单状态
 *
 * @author fan
 * @return
 * @date 2020/06/16 15:45
 */
public enum MenuStatus implements BaseEnum {

    NORMAL("0", "正常"),

    HIDDEN("1", "隐藏");

    private final String value;

    private final String note;

    MenuStatus(String value, String note) {
        this.value = value;
        this.note = note;
    }

    @Override
    public String getValue() {
        return this.value;
    }

    public String getNote() {
        return this.note;
    }

}