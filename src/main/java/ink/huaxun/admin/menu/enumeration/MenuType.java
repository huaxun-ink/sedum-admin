package ink.huaxun.admin.menu.enumeration;

import ink.huaxun.core.enumeration.BaseEnum;

/**
 * 菜单类型
 *
 * @author fan
 * @date 2020/06/16 15:45
 */
public enum MenuType implements BaseEnum {

    MENU("C", "菜单"),

    BUTTON("A", "按钮");

    private final String value;

    private final String note;

    MenuType(String value, String note) {
        this.value = value;
        this.note = note;
    }

    @Override
    public String getValue() {
        return this.value;
    }

    public String getNote() {
        return this.note;
    }

}