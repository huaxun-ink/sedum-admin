package ink.huaxun.admin.menu.entity;

import ink.huaxun.admin.menu.enumeration.MenuStatus;
import ink.huaxun.admin.menu.enumeration.MenuType;
import ink.huaxun.core.annotation.Query;
import ink.huaxun.core.entity.TreeEntity;
import ink.huaxun.core.enumeration.QueryEnum;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * 菜单表 sys_menu
 *
 * @author fan
 */
@Data
public class SysMenu extends TreeEntity<SysMenu> {

    private static final long serialVersionUID = -1612100927547119244L;

    /**
     * 菜单名称
     */
    @NotBlank(message = "菜单名称不能为空")
    @Query(type = QueryEnum.LIKE)
    private String name;

    /**
     * 路由地址
     */
    private String path;

    /**
     * 组件路径
     */
    private String component;

    /**
     * 编号
     */
    private String codeNum;

    /**
     * 类型（C菜单 A按钮）
     */
    @NotNull(message = "菜单类型不能为空")
    @Query(type = QueryEnum.EQ)
    private MenuType type;

    /**
     * 菜单状态（0显示 1隐藏）
     */
    @Query(type = QueryEnum.EQ)
    private MenuStatus status;

    /**
     * 权限字符串
     */
    private String permission;

    /**
     * 菜单图标
     */
    private String icon;

}
