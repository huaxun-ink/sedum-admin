package ink.huaxun.admin.menu.service;

import ink.huaxun.admin.menu.entity.SysMenu;
import ink.huaxun.admin.menu.mapper.SysMenuMapper;
import ink.huaxun.admin.menu.vo.TreeVO;
import ink.huaxun.core.service.BaseService;
import ink.huaxun.core.util.TreeUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author zhaogang
 * @description 菜单
 * @date 2023/3/6 11:12
 */
@Service
public class SysMenuService extends BaseService<SysMenuMapper, SysMenu> {

    /**
     * 菜单权限判断条件
     */
    private static final String MENU = ":menu:";

    @Override
    public void save(SysMenu menu) {
        menu.setParentIds(TreeUtil.getParentIds(menu));
        super.save(menu);
    }

    @Override
    public void update(SysMenu menu) {
        menu.setParentIds(TreeUtil.getParentIds(menu));
        super.update(menu);
    }

    /**
     * 校验菜单名称是否被使用
     */
    public boolean checkNameUse(SysMenu menu) {
        return mapper.checkNameUse(menu) > 0;
    }

    /**
     * 构建前端所需要树结构
     */
    public List<SysMenu> buildMenuTree(List<SysMenu> menuList) {
        return TreeUtil.getRootList(menuList);
    }

    public List<TreeVO> buildMenuTreeForSelect(List<SysMenu> menuList) {
        List<SysMenu> menuTree = buildMenuTree(menuList);
        return menuTree.stream().map(menu -> {
            TreeVO treeVO = new TreeVO();
            BeanUtils.copyProperties(menu, treeVO);
            return treeVO;
        }).collect(Collectors.toList());
    }

    /**
     * 根据用户角色获取菜单
     */
    public List<SysMenu> getMenuByUserRole(Long userId) {
        return mapper.getMenuByUserRole(userId);
    }

    /**
     * 根据用户角色获取按钮
     */
    public List<SysMenu> getButtonByUserRole(Long userId) {
        return mapper.getButtonByUserRole(userId);
    }

    /**
     * 根据id获取菜单
     */
    public List<SysMenu> getMenuByIds(Collection<Long> ids) {
        if (CollectionUtils.isEmpty(ids)) {
            return null;
        }
        return mapper.getMenuByIds(ids);
    }

    /**
     * 根据id获取按钮
     */
    public List<SysMenu> getButtonByIds(Collection<Long> ids) {
        if (CollectionUtils.isEmpty(ids)) {
            return null;
        }
        return mapper.getButtonByIds(ids);
    }

    /**
     * 判断是否为菜单及其上级节点
     */
    public boolean checkMenu(Long id) {
        SysMenu menu = super.get(id);
        String permission = menu.getPermission();
        if (this.checkPermission(permission)) {
            return true;
        }
        List<SysMenu> list = super.getList();
        return this.hasChildPermission(list, menu);
    }

    /**
     * 判断是否为菜单权限
     */
    private boolean checkPermission(String permission) {
        return StringUtils.isNotBlank(permission) && permission.contains(MENU);
    }

    /**
     * 递归判断下级节点中是否存在菜单权限
     */
    private boolean hasChildPermission(List<SysMenu> list, SysMenu menu) {
        for (SysMenu child : list) {
            if (!menu.getId().equals(child.getParentId())) {
                continue;
            }
            if (this.checkPermission(menu.getPermission())) {
                return true;
            }
            if (this.hasChildPermission(list, child)) {
                return true;
            }
        }
        return false;
    }

}