package ink.huaxun.admin.menu.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 描述:
 *
 * @author fan
 * @date 2020-06-18 11:12
 */
@Data
public class TreeVO implements Serializable {

    private static final long serialVersionUID = -3431090749859047205L;

    private Long id;

    private Long parentId;

    private String name;

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private String icon;

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private String path;

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private List<TreeVO> children;

}
