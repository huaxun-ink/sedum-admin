package ink.huaxun.admin.menu.controller;

import ink.huaxun.admin.menu.entity.SysMenu;
import ink.huaxun.admin.menu.enumeration.MenuStatus;
import ink.huaxun.admin.menu.enumeration.MenuType;
import ink.huaxun.admin.menu.mapper.SysMenuMapper;
import ink.huaxun.admin.menu.service.SysMenuService;
import ink.huaxun.admin.menu.vo.TreeVO;
import ink.huaxun.admin.role.service.SysRoleService;
import ink.huaxun.authority.annotation.RequiresPermissions;
import ink.huaxun.core.controller.BaseController;
import ink.huaxun.core.enumeration.OrderEnum;
import ink.huaxun.core.vo.Page;
import ink.huaxun.core.vo.Result;
import ink.huaxun.core.vo.Sort;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * @author zhaogang
 * @description 菜单
 * @date 2023/3/6 11:13
 */
@RestController
@RequestMapping("/sys/menu")
public class SysMenuController extends BaseController<SysMenuService, SysMenuMapper, SysMenu> {

    @Resource
    private SysRoleService roleService;

    @RequiresPermissions({"sys:menu:list", "sys:role:list"})
    @GetMapping
    @Override
    public Result list(SysMenu menu, Page page, Sort sort) {
        List<SysMenu> list = service.getSort(menu, sort);
        List<SysMenu> tree = service.buildMenuTree(list);
        return Result.ok(tree);
    }

    /**
     * 获取全量菜单下拉选择树
     */
    @RequiresPermissions
    @GetMapping("/tree/select")
    public Result treeSelect(SysMenu menu) {
        List<Sort> sorts = new ArrayList<>();
        sorts.add(new Sort("parentId", OrderEnum.ASC));
        sorts.add(new Sort("orderNum", OrderEnum.ASC));
        menu.setType(MenuType.MENU);
        List<SysMenu> menuData = service.getSort(menu, sorts);
        List<TreeVO> menus = service.buildMenuTreeForSelect(menuData);
        return Result.ok(menus);
    }

    @RequiresPermissions
    @PostMapping
    @Override
    public Result add(@RequestBody SysMenu menu) {
        if (service.checkNameUse(menu)) {
            return Result.conflict("新增菜单'" + menu.getName() + "'失败，菜单名称已存在");
        }
        return super.add(menu);
    }

    @RequiresPermissions
    @PutMapping
    @Override
    public Result edit(@RequestBody SysMenu menu) {
        if (service.checkNameUse(menu)) {
            return Result.conflict("修改菜单'" + menu.getName() + "'失败，菜单名称已存在");
        }
        if (MenuStatus.HIDDEN == menu.getStatus() && service.checkMenu(menu.getId())) {
            return Result.conflict("禁止隐藏菜单及其上级节点！");
        }
        return super.edit(menu);
    }

    @RequiresPermissions
    @DeleteMapping("/{id}")
    @Override
    public Result remove(@PathVariable Long id) {
        if (service.checkMenu(id)) {
            return Result.conflict("禁止删除菜单及其上级节点！");
        }
        if (roleService.checkRoleUse(id)) {
            return Result.conflict("菜单已被角色绑定，请到对应角色中进行删除！");
        }
        return super.remove(id);
    }

}