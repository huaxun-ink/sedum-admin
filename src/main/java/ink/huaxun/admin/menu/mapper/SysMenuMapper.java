package ink.huaxun.admin.menu.mapper;

import ink.huaxun.admin.menu.entity.SysMenu;
import ink.huaxun.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;

/**
 * 菜单表 数据层
 *
 * @author fan
 */
@Repository
public interface SysMenuMapper extends BaseMapper<SysMenu> {

    /**
     * 校验菜单名称是否被使用
     *
     * @return 结果
     */
    Integer checkNameUse(SysMenu menu);

    /**
     * 根据用户角色获取菜单
     *
     * @author zhaogang
     * @date 2020-06-16 16:48
     */
    List<SysMenu> getMenuByUserRole(Long userId);

    /**
     * 根据用户角色获取按钮
     *
     * @author zhaogang
     * @date 2020-06-16 16:48
     */
    List<SysMenu> getButtonByUserRole(Long userId);

    /**
     * 根据id获取菜单
     *
     * @author zhaogang
     * @date 2020-07-16 14:39
     */
    List<SysMenu> getMenuByIds(Collection<Long> ids);

    /**
     * 根据id获取按钮
     *
     * @author zhaogang
     * @date 2020-07-16 14:39
     */
    List<SysMenu> getButtonByIds(Collection<Long> ids);

}
