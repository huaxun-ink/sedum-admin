package ink.huaxun.admin.organization.service;

import ink.huaxun.admin.organization.entity.SysOrganization;
import ink.huaxun.admin.organization.mapper.SysOrganizationMapper;
import ink.huaxun.core.service.BaseService;
import ink.huaxun.core.util.TreeUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author zhaogang
 * @description 机构
 * @date 2023/3/6 11:14
 */
@Service
public class SysOrganizationService extends BaseService<SysOrganizationMapper, SysOrganization> {

    /**
     * 构建前端所需要树结构
     *
     * @param organizationList 菜单列表
     * @return 树结构列表
     */
    public List<SysOrganization> buildTree(List<SysOrganization> organizationList) {
        return TreeUtil.getRootList(organizationList);
    }

    public void updateOrganizationName(Long organizationId, String organizationName) {
        mapper.updateOrganizationName(organizationId, organizationName);
    }

    public boolean checkOrganizationUse(Long id, boolean stop) {
        return mapper.checkOrganizationUse(id, stop) > 0;
    }

    @Override
    public void remove(Long id) {
        super.remove(id);
        this.updateOrganizationName(id, StringUtils.EMPTY);
    }

    @Override
    public void save(SysOrganization organization) {
        organization.setParentIds(TreeUtil.getParentIds(organization));
        super.save(organization);
    }

    @Override
    public void update(SysOrganization organization) {
        organization.setParentIds(TreeUtil.getParentIds(organization));
        super.update(organization);
        this.updateOrganizationName(organization.getId(), organization.getName());
    }

    public String getChildIds(Long id) {
        return mapper.getChildIds(id);
    }

    public boolean checkNameUse(SysOrganization organization) {
        return mapper.checkNameUse(organization) > 0;
    }

}