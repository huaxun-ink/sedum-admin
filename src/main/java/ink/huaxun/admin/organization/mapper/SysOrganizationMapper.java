package ink.huaxun.admin.organization.mapper;

import ink.huaxun.admin.organization.entity.SysOrganization;
import ink.huaxun.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * @author fan
 */
@Repository
public interface SysOrganizationMapper extends BaseMapper<SysOrganization> {

    /**
     * 更新机构名称
     */
    void updateOrganizationName(Long organizationId, String organizationName);

    /**
     * 校验机构是否被使用
     */
    Integer checkOrganizationUse(Long id, boolean stop);

    /**
     * 获取所有子级id
     */
    String getChildIds(Long id);


    /**
     * 获取是否有重名
     *
     * @param organization
     * @return
     */
    Integer checkNameUse(SysOrganization organization);
}
