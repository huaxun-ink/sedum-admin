package ink.huaxun.admin.organization.entity;

import ink.huaxun.admin.organization.enumeration.OrganizationStatus;
import ink.huaxun.core.annotation.Query;
import ink.huaxun.core.entity.TreeEntity;
import ink.huaxun.core.enumeration.QueryEnum;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author xrn
 */
@Data
public class SysOrganization extends TreeEntity<SysOrganization> {

    private static final long serialVersionUID = -1229253207962211538L;

    /**
     * 名字
     */
    @NotBlank(message = "名字不能为空！")
    @Query(type = QueryEnum.LIKE)
    private String name;

    /**
     * 编码
     */
    @Query(type = QueryEnum.EQ)
    private String code;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 负责人
     */
    private String leader;

    /**
     * 电话
     */
    private String phone;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 部门状态
     */
    @Query(type = QueryEnum.EQ)
    private OrganizationStatus status;

}
