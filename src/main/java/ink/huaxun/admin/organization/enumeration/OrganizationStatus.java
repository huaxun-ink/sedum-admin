package ink.huaxun.admin.organization.enumeration;

import ink.huaxun.core.enumeration.BaseEnum;

public enum OrganizationStatus implements BaseEnum {

    NORMAL("0", "正常"),

    STOP("1", "停用");

    private final String value;

    private final String note;

    OrganizationStatus(String value, String note) {
        this.value = value;
        this.note = note;
    }

    @Override
    public String getValue() {
        return this.value;
    }

    public String getNote() {
        return this.note;
    }

}
