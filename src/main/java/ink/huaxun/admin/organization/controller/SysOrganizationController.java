package ink.huaxun.admin.organization.controller;

import ink.huaxun.admin.organization.entity.SysOrganization;
import ink.huaxun.admin.organization.enumeration.OrganizationStatus;
import ink.huaxun.admin.organization.mapper.SysOrganizationMapper;
import ink.huaxun.admin.organization.service.SysOrganizationService;
import ink.huaxun.admin.user.service.SysUserService;
import ink.huaxun.authority.annotation.RequiresPermissions;
import ink.huaxun.core.controller.BaseController;
import ink.huaxun.core.vo.Page;
import ink.huaxun.core.vo.Result;
import ink.huaxun.core.vo.Sort;

import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author zhaogang
 * @description 机构
 * @date 2023/3/6 11:15
 */
@RestController
@RequestMapping("/sys/organization")
public class SysOrganizationController extends BaseController<SysOrganizationService, SysOrganizationMapper, SysOrganization> {

    @Resource
    private SysUserService userService;

    @RequiresPermissions({"sys:organization:list", "sys:user:list", "sys:role:list"})
    @GetMapping
    @Override
    public Result list(SysOrganization organization, Page page, Sort sort) {
        List<SysOrganization> list = service.getSort(organization, sort);
        List<SysOrganization> tree = service.buildTree(list);
        return Result.ok(tree);
    }

    @RequiresPermissions
    @GetMapping("/{id}")
    @Override
    public Result get(@PathVariable Long id) {
        return Result.ok(service.get(id));
    }

    @RequiresPermissions
    @PostMapping
    @Override
    public Result add(@RequestBody SysOrganization organization) {
        if (service.checkNameUse(organization)) {
            return Result.conflict("新增组织机构'" + organization.getName() + "'失败，组织名称已存在");
        }
        return super.add(organization);
    }

    @RequiresPermissions
    @PutMapping
    @Override
    public Result edit(@RequestBody SysOrganization organization) {
        boolean stop = organization.getStatus() == OrganizationStatus.STOP;
        Long id = organization.getId();
        if (stop && userService.checkOrganizationUse(id)) {
            return Result.conflict("部门下存在用户，不能停用！");
        }
        if (service.checkNameUse(organization)) {
            return Result.conflict("修改组织机构'" + organization.getName() + "'失败，组织名称已存在");
        }
        if (stop && service.checkOrganizationUse(id, true)) {
            return Result.conflict("存在下级部门，不能停用！");
        }
        return super.edit(organization);
    }

    @RequiresPermissions
    @DeleteMapping("/{id}")
    @Override
    public Result remove(@PathVariable Long id) {
        if (userService.checkOrganizationUse(id)) {
            return Result.conflict("部门下存在用户，不能删除！");
        }
        if (service.checkOrganizationUse(id, false)) {
            return Result.conflict("存在下级部门，不能删除！");
        }
        return super.remove(id);
    }

}
