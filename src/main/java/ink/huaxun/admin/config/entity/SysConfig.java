package ink.huaxun.admin.config.entity;

import ink.huaxun.core.entity.BaseEntity;
import org.hibernate.validator.constraints.Length;
import lombok.Data;

/**
* @author fix
* @description 配置
* @date 2023-04-17 16:46:24
*/
@Data
public class SysConfig extends BaseEntity {

    /**
    * 标题
    */
    @Length(max = 255)
    private String title;

    /**
    * Logo
    */
    @Length(max = 255)
    private String logo;

    /**
    * 背景图
    */
    @Length(max = 255)
    private String backgroundImage;

    /**
    * 风格
    */
    @Length(max = 255)
    private String style;

    /**
    * 其他配置
    */
    @Length(max = 255)
    private String otherConfigurations;

}
