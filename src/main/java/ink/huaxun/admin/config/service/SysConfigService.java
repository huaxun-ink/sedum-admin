package ink.huaxun.admin.config.service;

import ink.huaxun.admin.config.entity.SysConfig;
import ink.huaxun.admin.config.mapper.SysConfigMapper;
import ink.huaxun.core.service.BaseService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author fix
 * @description 配置
 * @date 2023-04-17 16:46:23
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class SysConfigService extends BaseService<SysConfigMapper, SysConfig> {

    public SysConfig getSysConfig() {
        List<SysConfig> list = getList();
        return list.get(0);
    }

}