package ink.huaxun.admin.config.mapper;

import ink.huaxun.admin.config.entity.SysConfig;
import ink.huaxun.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
* @author fix
* @description 配置
* @date 2023-04-17 16:46:23
*/
@Repository
public interface SysConfigMapper extends BaseMapper<SysConfig> {

}