
package ink.huaxun.admin.config.controller;

import ink.huaxun.admin.config.entity.SysConfig;
import ink.huaxun.admin.config.service.SysConfigService;
import ink.huaxun.admin.config.mapper.SysConfigMapper;
import ink.huaxun.core.controller.BaseController;
import ink.huaxun.core.vo.Result;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author fix
 * @description 配置
 * @date 2023-04-17 16:46:23
 */
@RestController
@RequestMapping("/sys/config")
public class SysConfigController extends BaseController<SysConfigService, SysConfigMapper, SysConfig> {

    @GetMapping("/content")
    public Result getSysConfig() {
        return Result.ok(service.getSysConfig());
    }

}