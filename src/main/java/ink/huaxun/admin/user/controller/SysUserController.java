package ink.huaxun.admin.user.controller;

import ink.huaxun.admin.user.entity.SysUser;
import ink.huaxun.admin.user.mapper.SysUserMapper;
import ink.huaxun.admin.user.service.SysUserService;
import ink.huaxun.admin.user.util.PasswordUtil;
import ink.huaxun.admin.user.vo.UserVO;
import ink.huaxun.authority.annotation.RequiresPermissions;
import ink.huaxun.core.controller.BaseController;
import ink.huaxun.core.vo.Data;
import ink.huaxun.core.vo.Page;
import ink.huaxun.core.vo.Result;
import ink.huaxun.core.vo.Sort;
import ink.huaxun.excel.util.ReadExcelUtils;
import ink.huaxun.excel.util.WriteExcelUtils;
import ink.huaxun.purview.vo.DataScope;
import ink.huaxun.util.UserUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author zhaogang
 * @description 用户
 * @date 2023/3/6 17:29
 */
@RestController
@RequestMapping("/sys/user")
public class SysUserController extends BaseController<SysUserService, SysUserMapper, SysUser> {

    @RequiresPermissions
    @GetMapping
    @Override
    public Result list(SysUser user, Page page, Sort sort) {
        Data<SysUser> data = service.getPage(user, page, sort, DataScope.acquiesce());
        return Result.ok(data);
    }

    @RequiresPermissions
    @GetMapping("/{id}")
    @Override
    public Result get(@PathVariable Long id) {
        return Result.ok(service.getUser(id));
    }

    @RequiresPermissions
    @PostMapping
    @Override
    public Result add(@RequestBody SysUser user) {
        // TODO 明年（2021）改成userName
        if (StringUtils.isBlank(user.getLoginName()) && StringUtils.isBlank(user.getMobile())) {
            return Result.badRequest("用户名/手机号不能为空！");
        }
        if (service.checkAccountUse(user)) {
            return Result.conflict("登陆名或手机号已存在，请重新输入！");
        }
        return super.add(user);
    }

    @RequiresPermissions
    @PutMapping
    @Override
    public Result edit(@RequestBody SysUser user) {
        // 用户修改不能修改账号/手机号/密码
        user.setLoginName(null);
        user.setMobile(null);
        user.setPassword(null);
        return super.edit(user);
    }

    @RequiresPermissions
    @DeleteMapping("/{id}")
    @Override
    public Result remove(@PathVariable Long id) {
        if (service.checkAdmin(id)) {
            return Result.conflict("超级管理员无法删除!");
        }
        return super.remove(id);
    }

    /**
     * * @api {PUT} /sys/user/change/password 修改密码
     * * @apiGroup 用户
     * * @apiParam {String} originalPassword 原密码
     * * @apiParam {String} password 密码
     * * @apiSuccess (返回) {int} code 200 代表成功 500代表错误
     * * @apiSuccess (返回) {String} message 提示信息
     * * @apiSuccess (返回) {String} data token串
     * * @apiSuccess (权限) {map} permission sys:user:change:password
     */
    @RequiresPermissions
    @PutMapping("/change/password")
    public Result changePassword(@RequestBody UserVO user) {
        user.setId(UserUtil.getId());
        if (!service.checkPasswordRight(user)) {
            return Result.conflict("原密码错误!");
        }
        if (!PasswordUtil.PasswordCheck(user.getPassword())) {
            return Result.conflict("密码长度不少于8位且需包含字母、数字和特殊符号!");
        }
        service.changePassword(user);
        return Result.ok("密码修改成功!");
    }

    /**
     * * @api {PUT} /sys/user/reset/password 重置密码
     * * @apiGroup 用户
     * * @apiParam {Long} id 用户id
     * * @apiParam {String} password 密码
     * * @apiSuccess (返回) {int} code 200 代表成功 500代表错误
     * * @apiSuccess (返回) {String} message 提示信息
     * * @apiSuccess (返回) {String} data token串
     * * @apiSuccess (权限) {map} permission sys:user:reset:password
     */
    @RequiresPermissions
    @PutMapping("/reset/password")
    public Result resetPassword(@RequestBody UserVO user) {
        service.changePassword(user);
        return Result.ok("密码重置成功!");
    }

    /**
     * * @api {GET} /sys/user/own 个人详情
     * * @apiGroup 用户
     * * @apiSuccess (返回) {int} code 200 代表成功 401未登录 403无权限 500系统错误
     * * @apiSuccess (返回) {String} message 提示信息
     * * @apiSuccess (返回) {String} data 返回列表数据 json格式
     * * @apiSuccess (权限) {map} permission sys:user:own
     */
    @GetMapping("/own")
    public Result own() {
        return Result.ok(service.get(UserUtil.getId()));
    }

    /**
     * * @api {GET} /sys/user/all 所有用户
     * * @apiGroup 用户
     * * @apiSuccess (返回) {int} code 200 代表成功 401未登录 403无权限 500系统错误
     * * @apiSuccess (返回) {String} message 提示信息
     * * @apiSuccess (返回) {String} data 返回列表数据 json格式
     * * @apiSuccess (权限) {map} permission sys:user:all, sys:message:add
     */
    @RequiresPermissions({"sys:user:all", "sys:message:list"})
    @GetMapping("/all")
    public Result all() {
        List<SysUser> list = service.getList();
        return Result.ok(list);
    }


    @GetMapping("/export")
    public Result exportExcel(SysUser user) {
        List<SysUser> list = service.getList(user);
        return Result.ok("导出成功", WriteExcelUtils.writeExcel("用户列表.xlsx", SysUser.class, list));
    }


    @PostMapping("/import")
    public Result importExcel(HttpServletRequest request) {
        MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
        // 获取上传文件对象
        MultipartFile file = multipartRequest.getFile("file");
        List<SysUser> list = ReadExcelUtils.readOneSheetExcel(file, SysUser.class);
        return Result.ok("导入成功", list);
    }
}
