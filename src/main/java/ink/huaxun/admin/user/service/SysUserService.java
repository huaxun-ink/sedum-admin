package ink.huaxun.admin.user.service;

import ink.huaxun.admin.auth.vo.WechatVO;
import ink.huaxun.admin.organization.service.SysOrganizationService;
import ink.huaxun.admin.role.entity.SysRole;
import ink.huaxun.admin.user.entity.SysUser;
import ink.huaxun.admin.user.mapper.SysUserMapper;
import ink.huaxun.admin.user.vo.UserVO;
import ink.huaxun.core.service.BaseService;
import ink.huaxun.core.vo.Data;
import ink.huaxun.core.vo.Page;
import ink.huaxun.core.vo.Sort;
import ink.huaxun.security.digest.SHA256;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * @author zhaogang
 * @description 用户service
 * @date 2023/3/6 10:54
 */
@Service
public class SysUserService extends BaseService<SysUserMapper, SysUser> {

    /**
     * 管理员标识
     */
    private static final String ADMIN = "admin";

    /**
     * ids分隔符
     */
    private static final String SEPARATOR = ",";

    @Resource
    private SysOrganizationService organizationService;

    public void changePassword(UserVO user) {
        SysUser sysUser = new SysUser();
        BeanUtils.copyProperties(user, sysUser);
        String password = this.getPassword(sysUser.getPassword());
        sysUser.setPassword(password);
        sysUser.setPasswordUpdateTime(new Date());
        mapper.changePassword(sysUser);
    }

    public boolean checkPasswordRight(UserVO user) {
        String originalPassword = this.getPassword(user.getOriginalPassword());
        user.setOriginalPassword(originalPassword);
        return mapper.checkPasswordRight(user) > 0;
    }

    public boolean checkAccountUse(SysUser user) {
        return mapper.checkAccountUse(user) > 0;
    }

    public boolean checkAdmin(Long id) {
        SysUser user = super.get(id);
        return ADMIN.equals(user.getLoginName());
    }

    public boolean checkRoleUse(Long roleId) {
        return mapper.checkRoleUse(roleId) > 0;
    }

    public boolean checkOrganizationUse(Long organizationId) {
        return mapper.checkOrganizationUse(organizationId) > 0;
    }

    @Override
    public void save(SysUser user) {
        String password = this.getPassword(user.getPassword());
        user.setPassword(password);
        user.setOrganizationName(organizationService.get(user.getOrganizationId()).getName());

        super.save(user);

        for (Long roleId : user.getRoleIdList()) {
            mapper.saveUserRole(user.getId(), roleId);
        }
    }

    /**
     * 获取混淆后的密码
     */
    public String getPassword(String password) {
        return SHA256.digest(password);
    }

    @Override
    public void update(SysUser user) {
        if (user.getOrganizationId() != null) {
            user.setOrganizationName(organizationService.get(user.getOrganizationId()).getName());
        }

        super.update(user);
        Long id = user.getId();
        mapper.removeUserRole(id);

        List<Long> roleIdList = user.getRoleIdList();
        if (CollectionUtils.isEmpty(roleIdList)) {
            return;
        }
        for (Long roleId : roleIdList) {
            mapper.saveUserRole(id, roleId);
        }
    }

    @Override
    public void remove(Long id) {
        super.remove(id);
        mapper.removeUserRole(id);
    }

    public UserVO getUser(Long id) {
        SysUser sysUser = super.get(id);

        UserVO user = new UserVO();
        BeanUtils.copyProperties(sysUser, user);

        List<SysRole> userRoleList = mapper.getUserRoleList(user.getId());
        user.setRoleList(userRoleList);
        return user;
    }

    public Data<SysUser> getPage(Page page, Sort sort, SysUser user) {
        Long organizationId = user.getOrganizationId();
        if (organizationId == null) {
            return super.getPage(user, page, sort);
        }
        String ids = organizationService.getChildIds(organizationId);
        if (StringUtils.isBlank(ids)) {
            user.setOrganizationIds(String.valueOf(organizationId));
            return super.getPage(user, page, sort);
        }
        user.setOrganizationIds(ids + SEPARATOR + organizationId);
        return super.getPage(user, page, sort);
    }


    public List<SysUser> getByIds(String userIds) {
        return mapper.getByIds(userIds);
    }

    public SysUser findByLoginName(UserVO userVO) {
        return mapper.findByLoginName(userVO);
    }

    public SysUser findByMobile(String mobile) {
        return mapper.findByMobile(mobile);
    }

    public WechatVO findByOpenId(String openId) {
        return mapper.findByOpenId(openId);
    }

    public void saveUserWeChat(Long userId, String openId, String accessToken) {
        mapper.saveUserWeChat(userId, openId, accessToken);
    }

    public void updateUserWeChat(Long userId, String openId, String accessToken, Long id) {
        mapper.updateUserWeChat(userId, openId, accessToken, id);
    }

    public void changeWechat(UserVO user) {
        WechatVO wechatVO = this.findByOpenId(user.getOpenId());
        mapper.updateUserWeChat(user.getId(), user.getOpenId(), null, wechatVO.getId());
    }
}
