package ink.huaxun.admin.user.vo;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

/**
 * @author zhubo
 */
@Data
public class UserExportVo {

    @ExcelProperty("用户名")
    private String userName;

    /**
     * 登陆名
     */
    @ExcelProperty("登录名")
    private String loginName;

    /**
     * 手机
     */
    @ExcelProperty("手机号")
    private String mobile;

    /**
     * 机构名称
     */
    @ExcelProperty("机构名称")
    private String organizationName;
}
