package ink.huaxun.admin.user.vo;

import com.fasterxml.jackson.annotation.JsonProperty;
import ink.huaxun.admin.role.entity.SysRole;
import ink.huaxun.admin.user.enumeration.UserStatus;
import lombok.Data;

import java.util.List;

/**
 * 用户
 *
 * @author zhaogang
 * @date 2020-06-04 14:34
 */
@Data
public class UserVO {

    private Long id;

    /**
     * 用户名
     */
    private String userName;

    /**
     * 登陆名
     */
    private String loginName;

    private String post;

    /**
     * 手机
     */
    private String mobile;

    /**
     * 密码
     */
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String password;

    /**
     * 原密码
     */
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String originalPassword;

    /**
     * 机构id
     */
    private Long organizationId;

    /**
     * 机构id
     */
    private String organizationName;

    /**
     * 头像
     */
    private String avatar;

    /**
     * 状态
     */
    private UserStatus status;

    /**
     * 角色id
     */
    private Long roleId;

    private String openId;

    /**
     * 角色bean
     */
    private List<SysRole> roleList;


    private List<Long> roleIdList;
}
