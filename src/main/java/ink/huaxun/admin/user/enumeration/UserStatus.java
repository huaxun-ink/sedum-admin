package ink.huaxun.admin.user.enumeration;

import ink.huaxun.core.enumeration.BaseEnum;

/**
 * 用户状态
 *
 * @author zhaogang
 * @date 2020-06-03 9:35
 */
public enum UserStatus implements BaseEnum {

    NORMAL("0", "正常"),

    STOP("1", "停用");

    private final String value;

    private final String note;

    UserStatus(String value, String note) {
        this.value = value;
        this.note = note;
    }

    @Override
    public String getValue() {
        return this.value;
    }

    public String getNote() {
        return this.note;
    }

}
