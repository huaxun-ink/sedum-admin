package ink.huaxun.admin.user.mapper;

import ink.huaxun.admin.auth.vo.WechatVO;
import ink.huaxun.admin.role.entity.SysRole;
import ink.huaxun.admin.user.entity.SysUser;
import ink.huaxun.admin.user.vo.UserVO;
import ink.huaxun.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * @author zhaogang
 * @date 2020/6/8
 */
@Repository
public interface SysUserMapper extends BaseMapper<SysUser> {

    /**
     * 登录
     */
    SysUser login(SysUser user);

    SysUser appLogin(SysUser user);

    SysUser wechatLogin(SysUser user);

    /**
     * 校验帐户是否被使用
     */
    Integer checkAccountUse(SysUser user);

    /**
     * 修改密码
     */
    void changePassword(SysUser user);

    /**
     * 校验密码是否正确
     */
    Integer checkPasswordRight(UserVO user);

    /**
     * 保存用户角色
     */
    void saveUserRole(Long id, Long roleId);

    /**
     * 获取该用户的角色
     */
    List<SysRole> getUserRoleList(Long id);

    /**
     * 删除用户角色
     */
    void removeUserRole(Long id);

    /**
     * 校验角色是否被使用
     */
    Integer checkRoleUse(Long roleId);

    /**
     * 校验角色是否被使用
     */
    Integer checkOrganizationUse(Long organizationId);


    /**
     * 根据userIds获取批量用户
     */
    List<SysUser> getByIds(String userIds);

    /**
     * 根据用户登录名获取用户
     *
     * @param user 登录名
     * @return
     */
    SysUser findByLoginName(UserVO user);


    /**
     * 保存微信登录信息
     *
     * @param userId
     * @param openId
     * @param accessToken
     */
    void saveUserWeChat(Long userId, String openId, String accessToken);

    /**
     * 更新微信登录信息
     *
     * @param userId
     * @param openId
     * @param accessToken
     */
    void updateUserWeChat(Long userId, String openId, String accessToken, Long id);

    SysUser findByMobile(String mobile);


    WechatVO findByOpenId(String openId);
}
