package ink.huaxun.admin.user.entity;

import com.alibaba.excel.annotation.ExcelProperty;
import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonProperty;
import ink.huaxun.admin.organization.entity.SysOrganization;
import ink.huaxun.admin.user.enumeration.UserStatus;
import ink.huaxun.core.annotation.BindQuery;
import ink.huaxun.core.annotation.Query;
import ink.huaxun.core.annotation.Sensitive;
import ink.huaxun.core.entity.BaseEntity;
import ink.huaxun.core.enumeration.QueryEnum;
import ink.huaxun.core.enumeration.SensitiveType;
import ink.huaxun.excel.converter.EnumConverter;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.List;

/**
 * 用户
 *
 * @author zhaogang
 * @date 2020-06-04 14:34
 */
@Data
public class SysUser extends BaseEntity {

    /**
     * 用户名
     */
    @Query(type = QueryEnum.LIKE)
    @ExcelProperty("用户名")
    private String userName;

    /**
     * 登陆名
     */
    @Query(type = QueryEnum.LIKE)
    @ExcelProperty("登录名")
    private String loginName;

    /**
     * 手机
     */
    @Query(type = QueryEnum.LIKE)
    @Sensitive(value = SensitiveType.MOBILE)
    @ExcelProperty("手机号")
    private String mobile;

    /**
     * 密码
     */
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String password;

    /**
     * 机构id
     */
    @Query(type = QueryEnum.EQ)
    private Long organizationId;

    /**
     * 机构ids(非数据库字段)
     */
    @TableField(exist = false)
    @Query(type = QueryEnum.IN, relevance = "organizationId")
    private String organizationIds;

    /**
     * 机构名称
     */
    @BindQuery(entity = SysOrganization.class, field = "name", condition = "this.organization_id=id")
    @TableField(exist = false)
    @ExcelProperty("机构名称")
    private String organizationName;

    /**
     * 头像
     */
    private String avatar;

    /**
     * 状态
     */
    @Query(type = QueryEnum.EQ)
    @ExcelProperty(value = "用户状态", converter = EnumConverter.class)
    private UserStatus status;

    /**
     * 最后登录时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date lastLoginTime;

    /**
     * 密码修正时间
     */
    private Date passwordUpdateTime;

    /**
     * 最近一次登录ip
     */
    private String loginIp;


    @TableField(exist = false)
    private String openId;

    @TableField(exist = false)
    private List<Long> roleIdList;

}
