package ink.huaxun.admin.user.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author zhubo
 * @date 2021/3/29
 */
public class PasswordUtil {

    private static final String PASSWORD_REG = "^(?![0-9]+$)(?![^0-9]+$)(?![a-zA-Z]+$)(?![^a-zA-Z]+$)(?![a-zA-Z0-9]+$)[a-zA-Z0-9\\S]{8,}$";


    public static Boolean PasswordCheck(String password) {
        Pattern regex = Pattern.compile(PASSWORD_REG);
        Matcher matcher = regex.matcher(password);
        return matcher.matches();
    }
}