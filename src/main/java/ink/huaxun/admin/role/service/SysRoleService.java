package ink.huaxun.admin.role.service;

import ink.huaxun.admin.role.entity.SysRole;
import ink.huaxun.admin.role.mapper.SysRoleMapper;
import ink.huaxun.admin.role.vo.RoleVO;
import ink.huaxun.core.service.BaseService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author zhaogang
 * @description 角色
 * @date 2023/3/7 9:17
 */
@Service
public class SysRoleService extends BaseService<SysRoleMapper, SysRole> {

    // 管理员标识
    private static final String ADMIN = "admin";

    @Override
    public void save(SysRole role) {
        super.save(role);
        Long roleId = role.getId();
        for (Long menuId : role.getMenuList()) {
            mapper.saveRoleMenu(roleId, menuId);
        }
    }

    public RoleVO getRole(Long id) {
        SysRole sysRole = super.get(id);
        RoleVO role = new RoleVO();
        BeanUtils.copyProperties(sysRole, role);
        List<Long> menuList = mapper.getRoleMenu(id);
        List<Long> orgIdList = mapper.getRoleOrganization(id);
        role.setMenuList(menuList);
        role.setOrganizationIdList(orgIdList);
        return role;
    }

    @Override
    public void update(SysRole role) {
        super.update(role);

        Long roleId = role.getId();
        boolean admin = this.checkAdmin(roleId);
        mapper.removeRoleMenu(roleId, admin);

        for (Long menuId : role.getMenuList()) {
            mapper.saveRoleMenu(roleId, menuId);
        }
    }

    public void updateDataScope(SysRole role) {
        super.update(role);

        Long roleId = role.getId();
        mapper.removeRoleOrganization(roleId);
        if (role.getOrganizationIdList() == null) {
            return;
        }
        for (Long organizationId : role.getOrganizationIdList()) {
            mapper.saveRoleOrganization(roleId, organizationId);
        }
    }

    @Override
    public void remove(Long id) {
        // 删除角色和用户关系
        mapper.removeUserRole(id);
        // 删除角色
        super.remove(id);
    }

    /**
     * 校验角色名称是否被使用
     */
    public boolean checkNameUse(SysRole role) {
        return mapper.checkNameUse(role) > 0;
    }

    /**
     * 校验角色编号是否被使用
     */
    public boolean checkCodeUse(SysRole role) {
        return mapper.checkCodeUse(role) > 0;
    }

    /**
     * 校验角色是否为管理员角色
     */
    public boolean checkAdmin(Long id) {
        SysRole sysRole = super.get(id);
        return ADMIN.equals(sysRole.getCode());
    }

    /**
     * 校验角色编号是否被使用
     */
    public boolean checkRoleUse(Long menuId) {
        return mapper.checkRoleUse(menuId) > 0;
    }

    public List<Long> getRoleOrganization(Long roleId) {
        return mapper.getRoleOrganization(roleId);
    }

}
