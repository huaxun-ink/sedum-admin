package ink.huaxun.admin.role.controller;

import ink.huaxun.admin.role.entity.SysRole;
import ink.huaxun.admin.role.mapper.SysRoleMapper;
import ink.huaxun.admin.role.service.SysRoleService;
import ink.huaxun.admin.user.service.SysUserService;
import ink.huaxun.authority.annotation.RequiresPermissions;
import ink.huaxun.core.controller.BaseController;
import ink.huaxun.core.vo.Result;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author zhaogang
 * @description 角色
 * @date 2023/3/7 9:06
 */
@RestController
@RequestMapping("/sys/role")
public class SysRoleController extends BaseController<SysRoleService, SysRoleMapper, SysRole> {

    @Resource
    private SysUserService userService;

    @RequiresPermissions
    @PostMapping
    @Override
    public Result add(@RequestBody SysRole role) {
        if (service.checkNameUse(role)) {
            return Result.conflict("新增角色'" + role.getName() + "'失败，角色名称已存在");
        }
        return super.add(role);
    }

    @GetMapping("/{id}")
    @Override
    public Result get(@PathVariable Long id) {
        return Result.ok(service.getRole(id));
    }

    @RequiresPermissions
    @PutMapping
    @Override
    public Result edit(@RequestBody SysRole role) {
        // TODO code目前不能修改
        role.setCode(null);
        if (service.checkNameUse(role)) {
            return Result.conflict("修改角色'" + role.getName() + "'失败，角色名称已存在");
        }
        return super.edit(role);
    }

    @RequiresPermissions
    @DeleteMapping("/{id}")
    @Override
    public Result remove(@PathVariable Long id) {
        if (userService.checkRoleUse(id)) {
            return Result.conflict("角色已被用户使用，无法删除");
        }
        if (service.checkAdmin(id)) {
            return Result.conflict("超级管理员无法删除");
        }
        return super.remove(id);
    }

    @RequiresPermissions({"sys:role:all", "sys:user:list"})
    @GetMapping("/all")
    public Result all() {
        List<SysRole> list = service.getList();
        return Result.ok(list);
    }

    @RequiresPermissions
    @PutMapping("/data/scope")
    public Result dataScope(@RequestBody SysRole role) {
        service.updateDataScope(role);
        return Result.ok("数据权限添加成功");
    }

    /**
     * 校验角色名称唯一
     */
    @GetMapping("/check/name")
    public Result checkName(SysRole role) {
        if (service.checkNameUse(role)) {
            return Result.conflict("角色名称已存在");
        }
        return Result.ok();
    }

    /**
     * 校验角色编码唯一
     */
    @GetMapping("/check/code")
    public Result checkCode(SysRole role) {
        if (service.checkCodeUse(role)) {
            return Result.conflict("角色编码已存在");
        }
        return Result.ok();
    }

}
