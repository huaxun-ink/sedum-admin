package ink.huaxun.admin.role.mapper;

import ink.huaxun.admin.role.entity.SysRole;
import ink.huaxun.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 角色mapper
 *
 * @author zhubo
 * @date 2020/6/15
 */
@Repository
public interface SysRoleMapper extends BaseMapper<SysRole> {

    /**
     * 删除角色用户关系
     *
     * @param roleId 角色id
     */
    void removeUserRole(Long roleId);

    /**
     * 校验角色名称是否被使用
     *
     * @author zhaogang
     * @date 2020-06-19 11:29
     */
    Integer checkNameUse(SysRole role);

    /**
     * 校验角色编号是否被使用
     *
     * @author zhaogang
     * @date 2020-06-19 11:29
     */
    Integer checkCodeUse(SysRole role);

    /**
     * 保存角色菜单
     *
     * @author zhaogang
     * @date 2020-07-01 16:10
     */
    void saveRoleMenu(Long roleId, Long menuId);

    /**
     * 删除角色菜单
     *
     * @author zhaogang
     * @date 2020-07-01 16:10
     */
    void removeRoleMenu(Long roleId, boolean admin);

    /**
     * 查询角色菜单
     *
     * @author zhaogang
     * @date 2020-07-01 16:10
     */
    List<Long> getRoleMenu(Long roleId);

    /**
     * 保存角色下的数据权限
     *
     * @author zhaogang
     * @date 2020-07-01 16:10
     */
    void saveRoleOrganization(Long roleId, Long organizationId);

    /**
     * 删除角色下的数据权限
     *
     * @author zhaogang
     * @date 2020-07-01 16:10
     */
    void removeRoleOrganization(Long roleId);

    /**
     * 查询角色下的数据权限
     *
     * @author zhaogang
     * @date 2020-07-01 16:10
     */
    List<Long> getRoleOrganization(Long roleId);

    /**
     * 校验菜单是否被角色使用
     *
     * @author wxh
     * @date 2023-3-1 14:37:23
     */
    Integer checkRoleUse(Long menuId);

}
