package ink.huaxun.admin.role.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import ink.huaxun.core.annotation.Query;
import ink.huaxun.core.entity.BaseEntity;
import ink.huaxun.core.enumeration.QueryEnum;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 * 角色
 *
 * @author zhaogang
 * @date 2020-06-19 11:45
 */
@Data
public class SysRole extends BaseEntity {

    /**
     * 角色名
     */
    @Query(type = QueryEnum.LIKE)
    @NotBlank(message = "角色名称不能为空！")
    private String name;

    /**
     * 角色编码
     */
    @Query(type = QueryEnum.EQ)
    @NotBlank(message = "角色编码不能为空！")
    private String code;

    /**
     * 数据权限范围
     */
    @NotBlank(message = "数据范围不能为空！")
    private String dataScope;

    /**
     * 菜单列表
     */
    @TableField(exist = false)
    private List<Long> menuList;

    /**
     * 部门组（数据权限）
     */
    @TableField(exist = false)
    private List<Long> organizationIdList;

}
