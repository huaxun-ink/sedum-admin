package ink.huaxun.admin.role.vo;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 * 角色操作VO类
 *
 * @author wxh
 */
@Data
public class RoleVO {

    /**
     * id
     */
    private Long id;

    /**
     * 角色名
     */
    @NotBlank(message = "角色名称不能为空！")
    private String name;

    /**
     * 角色编码
     */
    @NotBlank(message = "角色编码不能为空！")
    private String code;

    /**
     * 数据范围
     */
//    @NotBlank(message = "数据范围不能为空！")
    private String dataScope;

    /**
     * 描述
     */
    private String remark;

    /**
     * 菜单列表
     */
    private List<Long> menuList;


    /**
     * 部门组（数据权限）
     */
    private List<Long> organizationIdList;

}
