package ink.huaxun.admin.auth.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

/**
 * 用户操作VO类
 *
 * @author zhubo
 */
@Data
public class WechatVO {

    /**
     * 用户名
     */
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private Long id;

    private Long userId;

    private String openId;

}
