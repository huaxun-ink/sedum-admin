package ink.huaxun.admin.auth.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import ink.huaxun.admin.menu.vo.TreeVO;
import lombok.Data;

import java.util.List;
import java.util.Map;

/**
 * 用户操作VO类
 */
@Data
public class UserVO {

    /**
     * 用户名
     */
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private String userName;

    /**
     * 登录名
     */
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private String loginName;

    /**
     * 手机号
     */
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private String mobile;

    /**
     * 密码
     */
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String password;

    /**
     * 图片验证码
     */
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String captcha;

    /**
     * 身份验证码(短信、语音)
     */
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private String identityCode;


    /**
     * 头像
     */
    private String avatar;
    /**
     * token令牌
     */
    private String token;

    /**
     * openId
     */
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String openId;


    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String accessToken;
    /**
     * 菜单列表
     */
    private List<TreeVO> menuList;

    /**
     * 按钮权限
     */
    private Map<String, Object> buttonMap;

}
