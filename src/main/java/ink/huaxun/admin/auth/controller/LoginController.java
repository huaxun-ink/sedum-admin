package ink.huaxun.admin.auth.controller;

import ink.huaxun.admin.auth.service.LoginService;
import ink.huaxun.admin.auth.vo.UserVO;
import ink.huaxun.admin.user.entity.SysUser;
import ink.huaxun.admin.user.enumeration.UserStatus;
import ink.huaxun.admin.user.service.SysUserService;
import ink.huaxun.core.annotation.NotValidated;
import ink.huaxun.core.vo.Result;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author zhaogang
 * @description 登录
 * @date 2023/3/6 11:13
 */
@RestController
public class LoginController {

    @Resource
    private LoginService loginService;

    @Resource
    private SysUserService userService;

    /**
     * * @api {POST} /login 登录方法
     * * @apiGroup 用户
     * * @apiParam {String} userName 用户名
     * * @apiParam {String} password 密码
     * * @apiParam {String} captcha 验证码
     * * @apiSuccess (返回) {int} code 200 代表成功 400代表参数缺失 401代表账号密码验证码错误
     * * @apiSuccess (返回) {String} message 提示信息
     * * @apiSuccess (返回) {String} data token串
     */

    @PostMapping("/login")
    public Result login(@RequestBody UserVO user) {
        //首先判断是不是微信登陆
        if (StringUtils.isNotBlank(user.getOpenId())) {
            return this.weChatLogin(user);
        }
        // 然后判断是不是移动端的登陆方式,如果没有图形验证码的话就是移动端登陆
        if (StringUtils.isBlank(user.getCaptcha())) {
            return this.appLogin(user);
        }
        //最后是PC登陆
        return this.pcLogin(user);
    }

    /**
     * * @api {GET} /captcha 登录验证码
     * * @apiGroup 用户
     * * @apiSuccess (返回) {int} code 200 代表成功 401代表账号密码错误
     * * @apiSuccess (返回) {String} message 提示信息
     * * @apiSuccess (返回) {String} data base64图片字符串
     */
    @GetMapping("/captcha")
    public Result captcha() {
        String base64 = loginService.getCaptcha();
        if (StringUtils.isBlank(base64)) {
            return Result.internalServerError("获取验证码出错");
        }
        return Result.ok((Object) base64);
    }


    /**
     * * @api {GET} /mobileCaptcha 获取手机登录验证码
     * * @apiGroup 用户
     * * @apiSuccess (返回) {int} code 200 代表成功 401代表账号密码错误
     * * @apiSuccess (返回) {String} message 提示信息
     * * @apiSuccess (返回) {String} code 验证码字符串
     */

    @PostMapping("/mobileCaptcha")
    public Result mobileCaptcha(@NotValidated @RequestBody UserVO user) {
//        loginService.getMobileCaptcha(user.getMobile());
        return Result.ok(loginService.getTestMobileCaptcha(user.getMobile()));
    }

    /**
     * * @api {POST} /change/password 用户变更/忘记密码
     * * @apiGroup PC用户注册
     * * @apiParam {String} mobile 手机号
     * * @apiParam {String} code 验证码
     * * @apiParam {String} password 更新密码
     * * @apiSuccess (返回) {int} code 200 代表成功 500代表错误
     * * @apiSuccess (返回) {String} message 提示信息
     * * @apiSuccess (返回) {String} data token串
     */

    @PutMapping("/change/password")
    public Result mobileChangePassword(@RequestBody UserVO user) {
        if (!loginService.checkMobileCaptcha(user.getMobile(), user.getIdentityCode())) {
            return Result.badRequest("手机验证码错误！");
        }
        ink.huaxun.admin.user.vo.UserVO query = new ink.huaxun.admin.user.vo.UserVO();
        BeanUtils.copyProperties(user, query);
        SysUser oldUser = userService.findByLoginName(query);
        oldUser.setPassword(user.getPassword());
        BeanUtils.copyProperties(oldUser, query);
        userService.changePassword(query);
        return Result.ok("密码修改成功!");
    }

    private Result weChatLogin(UserVO user) {
        if (StringUtils.isBlank(user.getLoginName()) && StringUtils.isBlank(user.getMobile())) {
            return Result.badRequest("登陆信息不能为空！");
        }
        SysUser sysUser = loginService.weChatLogin(user);
        if (sysUser.getStatus() == UserStatus.STOP) {
            return Result.unauthorized("帐户已经被停用，请联系管理员！");
        }
        loginService.afterLogin(sysUser);
        user = loginService.getUser(sysUser);
        return Result.ok(user);
    }

    private Result appLogin(UserVO user) {
        if (StringUtils.isBlank(user.getLoginName()) && StringUtils.isBlank(user.getMobile())) {
            return Result.badRequest("手机号不能为空！");
        }
        if (StringUtils.isBlank(user.getIdentityCode())) {
            return Result.badRequest("请填写验证码");
        }
        if (!loginService.checkMobileCaptcha(user.getMobile(), user.getIdentityCode())) {
            return Result.unauthorized("验证码错误！");
        }
        SysUser sysUser = loginService.appLogin(user);
        if (sysUser == null) {
            sysUser = loginService.sign(user);
        } else if (sysUser.getStatus() == UserStatus.STOP) {
            return Result.unauthorized("帐户已经被停用，请联系管理员！");
        }
        loginService.afterLogin(sysUser);
        user = loginService.getUser(sysUser);
        return Result.ok(user);
    }

    private Result pcLogin(UserVO user) {
        if (StringUtils.isBlank(user.getLoginName()) && StringUtils.isBlank(user.getMobile())) {
            return Result.badRequest("登陆名/手机号不能为空！");
        }
        if (!loginService.checkCaptcha(user.getCaptcha())) {
            return Result.unauthorized("验证码错误！");
        }
        SysUser sysUser = loginService.login(user);
        if (sysUser == null) {
            loginService.removeCaptcha(user.getCaptcha());
            return Result.unauthorized("用户名密码错误！");
        }
        if (sysUser.getStatus() == UserStatus.STOP) {
            return Result.unauthorized("帐户已经被停用，请联系管理员！");
        }
        loginService.afterLogin(sysUser);
        user = loginService.getUser(sysUser);
        return Result.ok(user);
    }
}
