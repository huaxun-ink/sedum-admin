package ink.huaxun.admin.auth.service;

import ink.huaxun.admin.auth.vo.UserVO;
import ink.huaxun.admin.auth.vo.WechatVO;
import ink.huaxun.admin.menu.entity.SysMenu;
import ink.huaxun.admin.menu.service.SysMenuService;
import ink.huaxun.admin.menu.vo.TreeVO;
import ink.huaxun.admin.organization.service.SysOrganizationService;
import ink.huaxun.admin.role.entity.SysRole;
import ink.huaxun.admin.role.service.SysRoleService;
import ink.huaxun.admin.user.entity.SysUser;
import ink.huaxun.admin.user.mapper.SysUserMapper;
import ink.huaxun.admin.user.service.SysUserService;
import ink.huaxun.core.service.BaseService;
import ink.huaxun.core.util.TreeUtil;
import ink.huaxun.purview.enumeration.DataScopeEnum;
import ink.huaxun.purview.vo.DataAccess;
import ink.huaxun.redis.util.RedisUtil;
import ink.huaxun.security.digest.SHA256;
import ink.huaxun.util.HttpServletUtil;
import ink.huaxun.util.IpUtil;
import ink.huaxun.util.JwtUtil;
import ink.huaxun.util.ImageCaptchaUtil;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author zhaogang
 * @description 登录
 * @date 2023/3/6 11:09
 */
@Service
public class LoginService extends BaseService<SysUserMapper, SysUser> {

    /**
     * redis的用户权限key
     */
    @Value("${redis.permission-user-id}")
    private String permissionUserIdKey;

    /**
     * redis的用户key
     */
    @Value("${redis.user-id}")
    private String userIdKey;

    /**
     * 验证码位数
     */
    private static final int CAPTCHA_LENGTH = 4;

    /**
     * 手机验证码位数
     */
    private static final int MOBILE_CAPTCHA_LENGTH = 6;

    /**
     * 验证码到期时间（秒）
     */
    private static final int EXPIRATION_TIME = 60;

    /**
     * 手机验证码到期时间（秒）
     */
    private static final int MOBILE_EXPIRATION_TIME = 5 * 60;

    @Resource
    private SysMenuService menuService;

    @Resource
    private SysUserService userService;

    @Resource
    private SysRoleService roleService;

    @Resource
    private SysOrganizationService organizationService;


    public SysUser login(UserVO user) {
        SysUser sysUser = new SysUser();
        BeanUtils.copyProperties(user, sysUser);
        sysUser.setPassword(SHA256.digest(user.getPassword()));
        return mapper.login(sysUser);
    }


    public SysUser appLogin(UserVO user) {
        SysUser sysUser = new SysUser();
        BeanUtils.copyProperties(user, sysUser);
        if (StringUtils.isNotBlank(user.getPassword())) {
            sysUser.setPassword(SHA256.digest(user.getPassword()));
        }
        return mapper.appLogin(sysUser);
    }

    public SysUser weChatLogin(UserVO user) {
        SysUser sysUser = new SysUser();
        BeanUtils.copyProperties(user, sysUser);
        sysUser = mapper.wechatLogin(sysUser);
        if (sysUser != null) {
            return sysUser;
        }
        return this.sign(user);
    }

    public void afterLogin(SysUser sysUser) {
        sysUser.setLastLoginTime(new Date());
        HttpServletRequest request = HttpServletUtil.getRequest();
        sysUser.setLoginIp(IpUtil.getIpAddress(request));
        this.update(sysUser);
    }

    public UserVO getUser(SysUser sysUser) {
        Long userId = sysUser.getId();

        UserVO user = new UserVO();
        BeanUtils.copyProperties(sysUser, user);

        user.setToken(JwtUtil.createToken(String.valueOf(userId)));
        // 写入权限
        Set<Long> buttonSet = this.getButtonSet(userId);
        List<SysMenu> buttonList = menuService.getButtonByIds(buttonSet);
        List<SysMenu> menuList = this.getMenuList(userId, buttonSet);
        // 菜单要求树结构
        if (!CollectionUtils.isEmpty(menuList)) {
            List<TreeVO> treeList = menuService.buildMenuTreeForSelect(menuList);
            user.setMenuList(treeList);
        }
        Map<String, Object> buttonMap = this.listToMap(buttonList);
        user.setButtonMap(buttonMap);
        this.setPermission(userId, menuList, buttonMap);
        this.setDataAccess(userId, userService.getUser(sysUser.getId()));
        return user;
    }

    /**
     * 获取按钮集合
     */
    private Set<Long> getButtonSet(Long userId) {
        List<SysMenu> buttonList = menuService.getButtonByUserRole(userId);
        return this.adapter(buttonList);
    }

    /**
     * 写入权限
     */
    private void setPermission(Long userId, List<SysMenu> menuList, Map<String, Object> buttonMap) {
        Map<String, Object> menuMap = this.listToMap(menuList);
        menuMap.putAll(buttonMap);
        String key = permissionUserIdKey + userId;
        RedisUtil.setHash(key, menuMap);
    }

    /**
     * 写入数据权限
     */
    private void setDataAccess(Long userId, ink.huaxun.admin.user.vo.UserVO user) {
        if (CollectionUtils.isEmpty(user.getRoleList())) {
            return;
        }
        DataAccess dataAccess = new DataAccess();
        StringBuilder organizationIds = new StringBuilder();
        for (SysRole role : user.getRoleList()) {
            DataScopeEnum dataScope = DataScopeEnum.getEnum(role.getDataScope());
            if (DataScopeEnum.ALL == dataScope) {
                dataAccess.setAll(true);
                break;
            }
            if (dataScope == DataScopeEnum.CUSTOM) {
                organizationIds.append(StringUtils.join(roleService.getRoleOrganization(user.getRoleId()), ",")).append(",");
                continue;
            }
            if (dataScope == DataScopeEnum.ORGANIZATION) {
                organizationIds.append(user.getOrganizationId()).append(",");
                continue;
            }
            if (dataScope == DataScopeEnum.ORGANIZATION_AND_CHILD) {
                String organizationChildIds = organizationService.getChildIds(user.getOrganizationId());
                organizationIds.append(user.getOrganizationId()).append(",");
                if (StringUtils.isNotBlank(organizationChildIds)) {
                    organizationIds.append(organizationChildIds).append(",");
                }
                continue;
            }
            if (dataScope == DataScopeEnum.SELF) {
                dataAccess.setUserId(String.valueOf(userId));
            }
        }
        int size = organizationIds.length();
        if (size > 0) {
            organizationIds.deleteCharAt(size - 1);
        }
        dataAccess.setOrganizationIds(organizationIds.toString());
        String key = userIdKey + userId;
        RedisUtil.delete(key);
        RedisUtil.set(key, dataAccess);
    }

    /**
     * 用于适配前端数据结构
     */
    private List<SysMenu> getMenuList(Long userId, Set<Long> buttonSet) {
        List<SysMenu> menuList = menuService.getMenuByUserRole(userId);
        Set<Long> ids = this.adapter(menuList);
        ids.addAll(buttonSet);
        return menuService.getMenuByIds(ids);
    }

    /**
     * 用于适配前端数据结构
     */
    private Set<Long> adapter(List<SysMenu> list) {
        Set<Long> ids = new HashSet<>();
        for (SysMenu menu : list) {
            ids.add(menu.getId());
            String[] parentIds = StringUtils.split(menu.getParentIds(), TreeUtil.SEPARATOR);
            if (ArrayUtils.isEmpty(parentIds)) {
                continue;
            }
            for (String id : parentIds) {
                ids.add(Long.valueOf(id));
            }
        }
        return ids;
    }

    /**
     * list转map
     */
    private Map<String, Object> listToMap(List<SysMenu> list) {
        if (CollectionUtils.isEmpty(list)) {
            return new HashMap<>();
        }
        return list.stream().collect(Collectors.toMap(SysMenu::getPermission, SysMenu::getPath, (key, value) -> value));
    }

    /**
     * 获取验证码
     */
    public String getCaptcha() {
        String code = RandomStringUtils.randomAlphanumeric(CAPTCHA_LENGTH);
        String lowerCaseCode = code.toLowerCase();
        String realKey = SHA256.digest(lowerCaseCode);
        System.out.println("验证码为:" + lowerCaseCode);
        RedisUtil.set(realKey, lowerCaseCode, EXPIRATION_TIME);
        return ImageCaptchaUtil.generate(lowerCaseCode);
    }

    /**
     * 校验验证码
     */
    public boolean checkCaptcha(String captcha) {
        String lowerCaseCaptcha = captcha.toLowerCase();
        String realKey = SHA256.digest(lowerCaseCaptcha);
        Object checkCode = RedisUtil.get(realKey);
        return checkCode != null && checkCode.equals(lowerCaseCaptcha);
    }

    public void removeCaptcha(String captcha) {
        String lowerCaseCaptcha = captcha.toLowerCase();
        String realKey = SHA256.digest(lowerCaseCaptcha);
        RedisUtil.delete(realKey);
    }

    public String getTestMobileCaptcha(String mobile) {
        String code = RandomStringUtils.randomNumeric(MOBILE_CAPTCHA_LENGTH);
        System.out.println("手机验证码为:" + code);
        RedisUtil.set(mobile, code, MOBILE_EXPIRATION_TIME);
        return code;
    }

    public boolean checkMobileCaptcha(String mobile, String captcha) {
        try {
            String code = RedisUtil.get(mobile).toString();
            return captcha.equals(code);
        } catch (NullPointerException e) {
            return false;
        }
    }

    public SysUser sign(UserVO user) {
        //确定这个手机号和openid在系统内存不存在
        SysUser sysUser = userService.findByMobile(user.getMobile());
        WechatVO wechatVO;
        if (StringUtils.isNotBlank(user.getOpenId())) {
            wechatVO = userService.findByOpenId(user.getOpenId());
        } else {
            wechatVO = null;
        }
        //如果手机号不存在则这个号需要注册
        if (sysUser == null) {
            sysUser = this.createUser(user);
        }
        //如果openId是不存在的，那么需要新增绑定
        if (wechatVO == null) {
            userService.saveUserWeChat(sysUser.getId(), user.getOpenId(), user.getAccessToken());
        }
        //如果openId存在的话，则需要重新换绑
        else {
            userService.updateUserWeChat(sysUser.getId(), user.getOpenId(), user.getAccessToken(), wechatVO.getId());
        }
        return sysUser;
    }

    public SysUser createUser(UserVO user) {
        if (StringUtils.isNotBlank(user.getPassword())) {
            String password = userService.getPassword(user.getPassword());
            user.setPassword(password);
        }
        SysUser sysUser = new SysUser();
        BeanUtils.copyProperties(user, sysUser);
        super.save(sysUser);
        return sysUser;
    }

}
