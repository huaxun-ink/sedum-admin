package ink.huaxun.admin.oss;

import com.aliyun.credentials.Client;
import com.aliyun.credentials.models.Config;
import ink.huaxun.redis.util.RedisUtil;
import ink.huaxun.util.SpringContextUtil;
import org.springframework.core.env.Environment;

import java.util.HashMap;
import java.util.Map;

public class AliOssHTTP {

    private static final Environment ENVIRONMENT = SpringContextUtil.getBean(Environment.class);

    /**
     * redis的用户key
     */
    private static final String ALIYUN_OSS_TOKEN = ENVIRONMENT.getProperty("redis.aliyun-oss-token", "");

    /**
     * ACCESS_KEY_ID
     */
    private static final String ACCESS_KEY_ID = ENVIRONMENT.getProperty("aliyun.oss.access-key-id");

    /**
     * ACCESS_KEY_SECRET
     */
    private static final String ACCESS_KEY_SECRET = ENVIRONMENT.getProperty("aliyun.oss.access-key-secret");

    /**
     * ROLE_SESSION_NAME
     */
    private static final String ROLE_SESSION_NAME = ENVIRONMENT.getProperty("aliyun.oss.role-session-name");

    /**
     * ROLE_ARN
     */
    private static final String ROLE_ARN = ENVIRONMENT.getProperty("aliyun.oss.role-arn");

    /**
     * TYPE
     */
    private static final String TYPE = "sts";

    /**
     * 请求临时token
     */
    public static Map<Object, Object> temporaryData() {
        Map<Object, Object> tokenMap = RedisUtil.getHashItem(ALIYUN_OSS_TOKEN);
        if (tokenMap.size() > 0) {
            return tokenMap;
        }
        Config config = new Config();
        config.type = TYPE;
        config.accessKeyId = ACCESS_KEY_ID;
        config.accessKeySecret = ACCESS_KEY_SECRET;
        config.roleArn = ROLE_ARN;
        config.roleSessionName = ROLE_SESSION_NAME;
        Client client = new Client(config);
        Map<String, Object> map = new HashMap<>();
        map.put("accessKeyId", client.getAccessKeyId());
        map.put("accessKeySecret", client.getAccessKeySecret());
        map.put("bearerToken", client.getBearerToken());
        map.put("securityToken", client.getSecurityToken());
        RedisUtil.setHash(ALIYUN_OSS_TOKEN, map, 1000 * 60 * 30);
        return RedisUtil.getHashItem(ALIYUN_OSS_TOKEN);
    }

}
