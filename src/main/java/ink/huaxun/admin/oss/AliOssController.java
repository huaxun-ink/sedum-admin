package ink.huaxun.admin.oss;

import ink.huaxun.core.vo.Result;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author weixuanhe
 * @description OSS
 * @date 2023/3/7 9:22
 */
@RestController()
@RequestMapping("/aliyun/oss")
public class AliOssController {

    @GetMapping("/token")
    public Result identification() {
        return Result.ok(AliOssHTTP.temporaryData());
    }

}